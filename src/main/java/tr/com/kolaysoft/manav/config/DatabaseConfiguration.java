package tr.com.kolaysoft.manav.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tr.com.kolaysoft.manav.domain.Grocery;
import tr.com.kolaysoft.manav.domain.Product;
import tr.com.kolaysoft.manav.domain.enumeration.Unit;
import tr.com.kolaysoft.manav.repository.GroceryRepository;
import tr.com.kolaysoft.manav.repository.ProductRepository;

@Configuration
@EnableJpaRepositories({ "tr.com.kolaysoft.manav.repository" })
@EnableTransactionManagement
public class DatabaseConfiguration {

    @Autowired
    private GroceryRepository groceryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Bean
    public CommandLineRunner initialCreateGrocery(){

        for (int i = 0; i < 10; i++) {
            groceryRepository.save(new Grocery("grocery"+i));
            productRepository.save(new Product("ekmek"+i, Unit.KG));
        }
        return null;
    }
}
