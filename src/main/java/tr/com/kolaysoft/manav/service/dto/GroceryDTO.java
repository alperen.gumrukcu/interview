package tr.com.kolaysoft.manav.service.dto;

import io.swagger.v3.oas.annotations.tags.Tag;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link tr.com.kolaysoft.manav.domain.Grocery} entity.
 */
public class GroceryDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GroceryDTO)) {
            return false;
        }

        GroceryDTO groceryDTO = (GroceryDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, groceryDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }


    @Override
    public String toString() {
        return "GroceryDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            '}';
    }
}
