package tr.com.kolaysoft.manav.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tr.com.kolaysoft.manav.domain.Grocery;
import tr.com.kolaysoft.manav.repository.GroceryRepository;
import tr.com.kolaysoft.manav.service.GroceryService;
import tr.com.kolaysoft.manav.service.dto.GroceryDTO;
import tr.com.kolaysoft.manav.service.dto.ProductDTO;
import tr.com.kolaysoft.manav.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link tr.com.kolaysoft.manav.domain.Grocery}.
 */
@RestController
@RequestMapping("/api")
@Tag( name = "Grocery Api Documentation")
public class GroceryResource {

    private final GroceryService groceryService;

    public GroceryResource(GroceryService groceryService) {
        this.groceryService = groceryService;
    }

    /**
     * {@code POST  /groceries} : Create a new grocery.
     *
     * @param groceryDTO the groceryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new groceryDTO, or with status {@code 400 (Bad Request)} if the grocery has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @Operation(summary = "New Grocery adding method")
    @PostMapping("/groceries")
    public ResponseEntity<Grocery> createGrocery(@Valid @RequestBody GroceryDTO groceryDTO) throws URISyntaxException {
        if (groceryDTO.getId() != null) {
            throw new BadRequestAlertException("A new grocery cannot already have an ID");
        }
        Grocery result = groceryService.save(groceryDTO);
        return ResponseEntity.created(new URI("/api/groceries/" + result.getId())).body(result);
    }

    /**
     * {@code PUT  /groceries} : Updates an existing grocery.
     *
     * @param groceryDTO the groceryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated groceryDTO,
     * or with status {@code 400 (Bad Request)} if the groceryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the groceryDTO couldn't be updated.
     */
    @Operation(summary = "Grocery updating method")
    @PutMapping("/groceries")
    public ResponseEntity<Grocery> updateGrocery(@Valid @RequestBody GroceryDTO groceryDTO) {
        if (groceryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id");
        }
        if (!groceryService.existsById(groceryDTO.getId())) {
            throw new BadRequestAlertException("Entity not found");
        }
        Grocery result = groceryService.save(groceryDTO);
        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code GET  /groceries} : get all the groceries.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of groceries in body.
     */
    @Operation(summary = "Grocery List method")
    @GetMapping("/groceries")
    public List<GroceryDTO> getAllGroceries() {
        return groceryService.findAll();
    }

    /**
     * {@code GET  /groceries/:id} : get the "id" grocery.
     *
     * @param id the id of the groceryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the groceryDTO, or with status {@code 404 (Not Found)}.
     */
    @Operation(summary = "Get grocery with id method")
    @GetMapping("/groceries/{id}")
    public ResponseEntity<GroceryDTO> getProduct(@PathVariable Long id) {
        Optional<GroceryDTO> groceryDTO = groceryService.findOne(id);
        if (groceryDTO.orElse(null) == null) {
            throw new BadRequestAlertException("Entity not found");
        }
        return ResponseEntity.ok(groceryDTO.orElse(null));
    }

    /**
     * {@code DELETE  /groceries/:id} : delete the "id" grocery.
     *
     * @param id the id of the groceryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @Operation(summary = "Deleted grocery method")
    @DeleteMapping("/groceries/{id}")
    public ResponseEntity<Void> deleteGrocery(@PathVariable Long id) {
        groceryService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
